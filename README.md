# TODOLIST Python

Voici notre projet Todolist en python


## Démarrage

Pour démarrer votre programme :

Executez la commande ``python3 manage.py runserver``

Pour avoir accès au site de notre Todolist :
 * Identifiant : ``test@exemple.com``
 * Mot de passe : ``testtest``


## Fabriqué avec

Les programmes/logiciels/ressources que nous avons utilisé pour développer notre projet

* [Python](https://www.python.org/) - Langage de programmation interprété
* [Django](https://www.djangoproject.com/) - Framework Python
* [Firebase](https://firebase.google.com/) -  Services d'hébergement et de bases de données en temps réel
* [Pyrebase](https://github.com/thisbejim/Pyrebase) - A simple python wrapper for the Firebase API.


## Auteurs

* **Boris Setti** _alias_ [@Boris.setti](https://gitlab.com/Boris.setti)
* **Abdellah Skoundri** _alias_ [@sk_abdellah](https://gitlab.com/sk_abdellah)



