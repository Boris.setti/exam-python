from django.shortcuts import render
from django.shortcuts import redirect
import pyrebase


config = {
    "apiKey": "AIzaSyB3_2_8CVxhf7uADAuiGcIN3ECLSAWqqoQ",
    "authDomain": "exam-python.firebaseapp.com",
    "databaseURL": "https://exam-python.firebaseio.com",
    "projectId": "exam-python",
    "storageBucket": "exam-python.appspot.com",
    "messagingSenderId": "442472105813",
    "appId": "1:442472105813:web:c480d9b95e84a9691211dc"
}

firebase = pyrebase.initialize_app(config)

auth = firebase.auth()

db = firebase.database()


def singIn(request):

    return render(request, "signIn.html")


def postsign(request):

    email = request.POST.get('email')
    passw = request.POST.get("pass")
    try:
        user = auth.sign_in_with_email_and_password(email, passw)
    except:
        message = "invalid cerediantials"
        return render(request, "signIn.html", {"msg": message})
    print(user)
    return redirect('/todolist')


def todolist(request):
    todoList = db.child("todo").get()
    data = todoList
    dataListe = []
    for liste in data.each():
        key = liste.key()
        msg = liste.val()
        result = {'key': key, 'msg': msg['msg']}
        dataListe.append(result)

    return render(request, "welcome.html", {'data': dataListe})


def addMsg(request):
    msg = request.POST.get('msg')
    data = {"msg": msg}
    db.child("todo").push(data)
    return redirect('/todolist/')


def delMsg(request):
    key = request.POST.get(('key'))
    db.child("todo").child(key).remove()
    return redirect('/todolist/')
